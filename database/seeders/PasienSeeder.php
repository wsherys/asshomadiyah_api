<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
//
use Faker\Factory as Faker;

class PasienSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $faker = Faker::create('id');
        for($i = 1; $i <= 50; $i++){
            // insert data ke table siswa menggunakan Faker
            \DB::table('Pasiens')->insert([
                'nomor_pasien' => $faker->randomNumber,
                'nama_lengkap' => $faker->name,
                'tempat_lahir' => $faker->state,
                'tgl_lahir' => $faker->date,
                'jenis_kelamin' => 'L/P',
                'agama' => 'agama',
                'pendidikan' => $faker->name,
                'pekerjaan' => $faker->name,
                'rt' => $faker->randomDigit,
                'rw' => $faker->randomDigit,
                'kelurahan' => $faker->citySuffix,
                'kecamatan' => $faker->streetSuffix,
                'kodepos' => $faker->postcode,
                'no_telepon' => $faker->phoneNumber,
                'jenis_pembayaran' => 'Tunai',
                'created_at' => $faker->date,
                'updated_at' => $faker->date,
            ]);
        }
        // php artisan db:seed --class=PasienSeeder
    }
}
