<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePermintaanPemeriksaanRadiologisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('request_radiologicals', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('id_pasien')->nullable();
            $table->string('pemeriksaan_radiologi')->nullable();
            $table->string('kategori_extremitas')->nullable();
            $table->string('no_rekam_medis')->nullable();
            $table->string('dokter_pengirim')->nullable();
            $table->string('dokter_pemeriksa')->nullable();
            $table->string('tindakan_operasi')->nullable();
            $table->string('no_foto_lama')->nullable();
            $table->string('no_foto_baru')->nullable();
            $table->string('gejala_klinis')->nullable();
            $table->string('hasil_lab')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('request_radiologicals');
    }
}
