<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClinicalLaboratories extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('labkliniks', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('id_pasien')->nullable();//nama_pasien,umur,alamat_lengkap,no_telepon
            $table->string('id_katkes')->nullable();//judul_katkes, nama_katkes
            $table->string('nomor_lab')->nullable();
            $table->string('nama_dokter')->nullable();
            $table->string('diagnosa')->nullable();
            $table->string('tgl_permintaan')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clinical_laboratories');
    }
}
