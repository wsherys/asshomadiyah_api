<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Pasien;
use Illuminate\Support\Facades\Validator;

class PasienController extends Controller
{
    // public function index()
    public function index()
    {
        // Pasien::truncate();
        if (request()->has('search')) {
            $select = Pasien::Where('nama_lengkap', 'LIKE', '%' . request('search') . "%")->get();
            //make response JSON
            return response()->json($select, 200);
        }else{
            //get data from table posts
            $posts = Pasien::orderBy('id', 'DESC')->get();
            //make response JSON
            return response()->json([
                'success' => true,
                'message' => 'List Data Pasien',
                'data'    => $posts
            ], 200);
        }
    }
    
    public function show($id)
    {
        //find post by ID
        $post = Pasien::findOrfail($id);

        //make response JSON
        return response()->json([
            'success' => true,
            'message' => 'Detail Data Pasien',
            'data'    => $post 
        ], 200);

    }
    
    public function store(Request $request)
    {
        //set validation
        $validator = Validator::make($request->all(), [
            'nama_lengkap'   => 'required'
        ]);
        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }
        $post = new Pasien;
        // $post->nomor_pasien = $request->input('nomor_pasien');
        $post->nama_lengkap = $request->input('nama_lengkap');
        $post->tempat_lahir = $request->input('tempat_lahir');
        $post->tgl_lahir = $request->input('tgl_lahir');
        $post->jenis_kelamin = $request->input('jenis_kelamin');
        $post->agama = $request->input('agama');
        $post->pendidikan = $request->input('pendidikan');
        $post->pekerjaan = $request->input('pekerjaan');
        $post->status = $request->input('status');
        $post->alamat_lengkap = $request->input('alamat_lengkap');
        $post->rt = $request->input('rt');
        $post->rw = $request->input('rw');
        $post->kelurahan = $request->input('kelurahan');
        $post->kecamatan = $request->input('kecamatan');
        $post->kodepos = $request->input('kodepos');
        $post->no_telepon = $request->input('no_telepon');
        $post->jenis_pembayaran = $request->input('jenis_pembayaran');
        //save to database
        $post->save();
        //success save to database
        if($post) {
            return response()->json([
                'success' => true,
                'message' => 'Pasien Created',
                'data'    => $post
            ], 201);
        } 
        //failed save to database
        return response()->json([
            'success' => false,
            'message' => 'Pasien Failed to Save',
        ], 409);
    }
    
   
    public function update(Request $request, $post)
    {
        $post = Pasien::find($post);
        if($post) {
            // $post->nomor_pasien = $request->nomor_pasien;
            $post->nama_lengkap = $request->nama_lengkap;
            $post->tempat_lahir = $request->tempat_lahir;
            $post->tgl_lahir = $request->tgl_lahir;
            $post->jenis_kelamin = $request->jenis_kelamin;
            $post->agama = $request->agama;
            $post->pendidikan = $request->pendidikan;
            $post->pekerjaan = $request->pekerjaan;
            $post->status = $request->status;
            $post->alamat_lengkap = $request->alamat_lengkap;
            $post->rt = $request->rt;
            $post->rw = $request->rw;
            $post->kelurahan = $request->kelurahan;
            $post->kecamatan = $request->kecamatan;
            $post->kodepos = $request->kodepos;
            $post->no_telepon = $request->no_telepon;
            $post->jenis_pembayaran = $request->jenis_pembayaran;
            //update
            $post->update();
        
            return response()->json([
                'success' => true,
                'message' => 'Pasien Updated',
                'data'    => $post  
            ], 200);
        }

        // data post not found
        return response()->json([
            'success' => false,
            'message' => 'Post Not Found',
        ], 404);

    }
    
   
    public function destroy($id)
    {
        //find post by ID
        $post = Pasien::findOrfail($id);

        if($post) {

            //delete post
            $post->delete();

            return response()->json([
                'success' => true,
                'message' => 'Post Deleted',
            ], 200);

        }

        //data post not found
        return response()->json([
            'success' => false,
            'message' => 'Post Not Found',
        ], 404);
    }
}
