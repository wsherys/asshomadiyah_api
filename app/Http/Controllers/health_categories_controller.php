<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

//import
use App\Models\health_categories;
use App\Models\kategoris;
use Illuminate\Support\Facades\Validator;

class health_categories_controller extends Controller
{
    //
    public function index(){
        if (request()->has('search')) {
            //
            $select = kategoris::select("*")
            ->where('kategori', 'LIKE', '%' . request('search') . "%")
            // ->groupBy("kategori")
            ->orderBy("kategori","ASC")
            ->get();

            //make response JSON
            return response()->json($select, 200);
        }
        elseif (request()->has('jenis')) {
            $select = kategoris::select("*")
            ->where('kategori', 'LIKE', '%' . request('jenis') . "%")
            ->orderBy("kategori","ASC")
            ->get();

            //make response JSON
            return response()->json($select, 200);
            // echo "jenis";
        }
        else{
            //get data from table posts
            $posts = kategoris::orderBy('id', 'DESC')->get();
            //make response JSON
            return response()->json([
                'success' => true,
                'message' => 'List Data Kategori Kesehatan',
                'data'    => $posts
            ], 200);
        }

    }
    
    public function show($id)
    {
        //find post by ID
        $post = kategoris::findOrfail($id);

        //make response JSON
        return response()->json([
            'success' => true,
            'message' => 'Detail Data Kategori Kesehatan',
            'data'    => $post 
        ], 200);

    }
    
    public function store(Request $request)
    {
        //set validation
        // $validator = Validator::make($request->all(), [
        //     'nama_lengkap'   => 'required'
        // ]);
        
        //response error validation
        // if ($validator->fails()) {
        //     return response()->json($validator->errors(), 400);
        // }
        
        $post = new kategoris;
        $post->kategori = $request->input('kategori');
        $post->jenis = $request->input('jenis');
        //save to database
        $post->save();

        //success save to database
        if($post) {
            return response()->json([
                'success' => true,
                'message' => 'Kategori Kesehatan Created',
                'data'    => $post
            ], 201);

        } 

        //failed save to database
        return response()->json([
            'success' => false,
            'message' => 'Kategori Kesehatan Failed to Save',
        ], 409);
    }
    
   
    // public function update(Request $request, Post $post)
    public function update(Request $request, $post)
    {

        $post = kategoris::find($post);
        if($post) {
            $post->kategori = $request->input('kategori');
            $post->jenis = $request->input('jenis');
            $post->update();

            return response()->json([
                'success' => true,
                'message' => 'Kategori Kesehatan Updated',
                'data'    => $post  
            ], 200);

        }
        else{
            // data post not found
            return response()->json([
                'success' => false,
                'message' => 'Post Not Found',
            ], 404);
        }
    }
    
   
    public function destroy($id)
    {
        //find post by ID
        $post = kategoris::findOrfail($id);

        if($post) {

            //delete post
            $post->delete();

            return response()->json([
                'success' => true,
                'message' => 'Post Deleted',
            ], 200);

        }

        //data post not found
        return response()->json([
            'success' => false,
            'message' => 'Post Not Found',
        ], 404);
    }
}
