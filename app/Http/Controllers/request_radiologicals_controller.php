<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
//import
use App\Models\request_radiologicals;
use Illuminate\Support\Facades\Validator;

class request_radiologicals_controller extends Controller
{
    //

    public function index() {
        //
        if (request()->has('search')) {
            $select = request_radiologicals::select("*")
            ->where('id_pasien', 'LIKE', '%' . request('search') . "%")
            // ->groupBy("kategori")
            ->orderBy("id_pasien","ASC")
            ->get();

            //make response JSON
            return response()->json($select, 200);
        }else{
            //get data from table posts
            $posts = request_radiologicals::orderBy('id', 'DESC')->get();
            //make response JSON
            return response()->json([
                'success' => true,
                'message' => 'List Data permintaan radiologi',
                'data'    => $posts
            ], 200);
        }
    }
    
    public function show($id)
    {
        //find post by ID
        $post = request_radiologicals::findOrfail($id);

        //make response JSON
        return response()->json([
            'success' => true,
            'message' => 'Detail Data permintaan radiologi',
            'data'    => $post 
        ], 200);

    }
    
    public function store(Request $request)
    { 
        $post = new request_radiologicals;
        $post->id_pasien = $request->input('id_pasien');
        $post->pemeriksaan_radiologi = $request->input('pemeriksaan_radiologi');
        $post->kategori_extremitas = $request->input('kategori_extremitas');
        $post->no_rekam_medis = $request->input('no_rekam_medis');
        $post->dokter_pengirim = $request->input('dokter_pengirim');
        $post->dokter_pemeriksa = $request->input('dokter_pemeriksa');
        $post->tindakan_operasi = $request->input('tindakan_operasi');
        $post->no_foto_lama = $request->input('no_foto_lama');
        $post->no_foto_baru = $request->input('no_foto_baru');
        $post->gejala_klinis = $request->input('gejala_klinis');
        $post->hasil_lab = $request->input('hasil_lab');

        //save to database
        $post->save();

        //success save to database
        if($post) {
            return response()->json([
                'success' => true,
                'message' => 'permintaan radiologi Created',
                'data'    => $post
            ], 201);

        } 

        //failed save to database
        return response()->json([
            'success' => false,
            'message' => 'permintaan radiologi Failed to Save',
        ], 409);
    }
    
   
    public function update(Request $request, $post)
    {

        $post = request_radiologicals::find($post);
        if($post) {
            // $post->kategori = $request->input('kategori');
            // $post->jenis = $request->input('jenis');
            $post->id_pasien = $request->input('id_pasien');
            $post->pemeriksaan_radiologi = $request->input('pemeriksaan_radiologi');
            $post->kategori_extremitas = $request->input('kategori_extremitas');
            $post->no_rekam_medis = $request->input('no_rekam_medis');
            $post->dokter_pengirim = $request->input('dokter_pengirim');
            $post->dokter_pemeriksa = $request->input('dokter_pemeriksa');
            $post->tindakan_operasi = $request->input('tindakan_operasi');
            $post->no_foto_lama = $request->input('no_foto_lama');
            $post->no_foto_baru = $request->input('no_foto_baru');
            $post->gejala_klinis = $request->input('gejala_klinis');
            $post->hasil_lab = $request->input('hasil_lab');
            $post->update();

            return response()->json([
                'success' => true,
                'message' => 'permintaan radiologi Updated',
                'data'    => $post  
            ], 200);

        }
        else{
            // data post not found
            return response()->json([
                'success' => false,
                'message' => 'Post Not Found',
            ], 404);
        }
    }
    
   
    public function destroy($id)
    {
        //find post by ID
        $post = request_radiologicals::findOrfail($id);

        if($post) {

            //delete post
            $post->delete();

            return response()->json([
                'success' => true,
                'message' => 'Post Deleted',
            ], 200);

        }

        //data post not found
        return response()->json([
            'success' => false,
            'message' => 'Post Not Found',
        ], 404);
    }


}
