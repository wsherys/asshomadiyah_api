<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

//import
use App\Models\clinical_laboratories;
use App\Models\labkliniks;
use Illuminate\Support\Facades\Validator;

class clinical_laboratories_controller extends Controller
{
    //
    public function index()
    {
        if (request()->has('search')) {
            $select = labkliniks::Where('id_pasien', 'LIKE', '%' . request('search') . "%")->get();
            //make response JSON
            return response()->json($select, 200);
        }else{
            //get data from table posts
            $posts = labkliniks::orderBy('id', 'DESC')->get();
            //make response JSON
            return response()->json([
                'success' => true,
                'message' => 'List Data Pasien',
                'data'    => $posts
            ], 200);
        }
    }
   
    
    public function show($id)
    {
        //find post by ID
        $post = labkliniks::findOrfail($id);

        //make response JSON
        return response()->json([
            'success' => true,
            'message' => 'Detail Data Lab Klinik',
            'data'    => $post 
        ], 200);

    }
    
    public function store(Request $request)
    {
        $post = new labkliniks;
        $post->id = $request->input('id');
        $post->id_pasien = $request->input('id_pasien');
        $post->id_katkes = $request->input('katkes');
        $post->nomor_lab = $request->input('nomor_lab');
        $post->nama_dokter = $request->input('nama_dokter');
        $post->diagnosa = $request->input('diagnosa');
        $post->tgl_permintaan = $request->input('tgl_permintaan');
        
        //save to database
        $post->save();

        //success save to database
        if($post) {
            return response()->json([
                'success' => true,
                'message' => 'Lab KLinik Created',
                'data'    => $post
            ], 201);

        } 

        //failed save to database
        return response()->json([
            'success' => false,
            'message' => 'Lab KLinik Failed to Save',
        ], 409);
    }
    
   
    // public function update(Request $request, Post $post)
    public function update(Request $request, $post)
    {
        $post = labkliniks::find($post);
        if($post) {
            $post->id_pasien = $request->input('id_pasien');
            $post->id_katkes = $request->input('katkes');
            $post->nomor_lab = $request->input('nomor_lab');
            $post->nama_dokter = $request->input('nama_dokter');
            $post->diagnosa = $request->input('diagnosa');
            $post->tgl_permintaan = $request->input('tgl_permintaan');
            $post->update();

            return response()->json([
                'success' => true,
                'message' => 'Lab KLinik Updated',
                'data'    => $post  
            ], 200);

        }
        else{
            // data post not found
            return response()->json([
                'success' => false,
                'message' => 'Post Not Found',
            ], 404);
        }
    }
    
   
    public function destroy($id)
    {
        //find post by ID
        $post = labkliniks::findOrfail($id);
        if($post) {
            //delete post
            $post->delete();

            return response()->json([
                'success' => true,
                'message' => 'Post Deleted',
            ], 200);
        }
        //data post not found
        return response()->json([
            'success' => false,
            'message' => 'Post Not Found',
        ], 404);
    }
}
